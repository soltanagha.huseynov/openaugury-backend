<?php

use Illuminate\Database\Seeder;
use App\Models\Interaction\ChallengeTemplate;

class ChallengeTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('challenge_templates')->delete();

        ChallengeTemplate::create([
            'text' => 'Task',
            'challenge_type' => 'writtenWork',
            'subtext' => '(insert your task description here)',
            'options' => '[]'
        ]);

        ChallengeTemplate::create([
            'text' => 'Multichoice',
            'challenge_type' => 'multiple',
            'subtext' => '(insert your question text here)',
            'options' => json_encode([['value' => "a", 'text' => "First answer"], ['value' => "b", 'text' => "Second answer"], ['value' => "c", 'text' => "Third answer"], ['value' => "d", 'text' => "Fourth answer"]], true),
            'correct' => '"a"'
        ]);
    }
}
