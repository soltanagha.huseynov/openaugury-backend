<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;
use League\Csv\Reader;

class NICommunityCentreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $dataSource = CachedDataServerFeatureSet::whereName('NI Community Centres')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            CachedDataServerFeatureSet::create([
              'name' => 'NI Community Centres',
              'owner' => 'Department of Education (NI)',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/community-centres',
              'data_server' => 'opendatani',
              'data_server_set_id' => '00000000-0000-0000-0000-000000000003'
            ]);
        }

        $dataSource = CachedDataServerFeatureSet::whereName('NI Community Centres')->first();

        $district = District::whereName('Northern Ireland 1')->first();
        $feature_type = FeatureSet::whereName('Community Centre')->first();
        $feature_type->districts()->sync([
            $district->id => [
                'data_server_set_id' => $dataSource->data_server_set_id,
                'data_server' => 'opendatani',
                'status' => 0
            ]
        ]);

        $communityCentresCsv = Reader::createFromPath(base_path() . '/resources/opendata/communitycentresdata.csv');
        $headerRow = $communityCentresCsv->fetchOne();
        $communityCentresCsv->setOffset(1);
        $n = 0;
        $communityCentresCsv->each(function ($row) use (&$n, $dataSource, $headerRow) {
            $n++;
            $feature = new CachedDataServerFeature;
            $feature->feature_id = $n;
            $feature->location = new Point($row[4], $row[3]);
            $feature->json = json_encode(array_combine($headerRow, $row));
            $feature->cached_data_server_feature_set_id = $dataSource->id;
            $feature->save();
            return true;
        });
    }
}
