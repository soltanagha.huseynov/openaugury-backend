<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;
use League\Csv\Reader;

class BelfastTreesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $dataSource = CachedDataServerFeatureSet::whereName('Belfast Trees')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            CachedDataServerFeatureSet::create([
              'name' => 'Belfast Trees',
              'owner' => 'Belfast City Council',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/belfast-trees',
              'data_server' => 'opendatani',
              'data_server_set_id' => '00000000-0000-0000-0000-000000000003'
            ]);
        }

        $dataSource = CachedDataServerFeatureSet::whereName('Belfast Trees')->first();

        $district = District::whereName('Northern Ireland 1')->first();
        $feature_type = FeatureSet::whereName('Belfast Trees')->first();
        $feature_type->districts()->sync([
            $district->id => [
                'data_server_set_id' => $dataSource->data_server_set_id,
                'data_server' => 'opendatani',
                'status' => 0
            ]
        ]);

        $treesCsv = Reader::createFromPath(base_path() . '/resources/opendata/odTrees.csv');
        $headerRow = $treesCsv->fetchOne();
        $treesCsv->setOffset(1);
        $n = 0;
        $treesCsv->each(function ($row) use (&$n, $dataSource, $headerRow) {
            $n++;
            $feature = new CachedDataServerFeature;
            $feature->feature_id = $n;
            $feature->location = new Point($row[4], $row[3]);
            $feature->json = json_encode(array_combine($headerRow, $row));
            $feature->cached_data_server_feature_set_id = $dataSource->id;
            $feature->save();
            return true;
        });
    }
}
