<?php

use App\Models\Users\User;
use App\Models\Users\Affiliation;
use App\Models\Features\District;
use App\Models\AbstractTier\Phenomenon;
use Illuminate\Database\Seeder;

class SampleUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(User::class, 10)->create();
        $affiliation = Affiliation::firstOrCreate(
            [
                'name' => 'Example School'
            ]
        );
        $affiliation->district_id = District::first()->id;
        $affiliation->save();

        $teacher = User::firstOrNew(
            [
                'forename' => 'Test',
                'surname' => 'Teacher',
                'email' => 'testteach@example.com',
            ]
        );
        $teacher['password'] = 'secret';
        $teacher['identifier'] = 'teacher101';
        $teacher['affiliation_id'] = $affiliation->id;

        $teacher->save();
        $teacher->assignRole('teacher');
        $teacher->save();

        $stormSurge = Phenomenon::whereName('Hurricane Storm Surge')->first();
        $combination = $stormSurge->numericalModels()->first()->combinations()->first();
        $affiliation->combinations()->attach($combination);
    }
}
