<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseFeatureChunksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_feature_chunks', function (Blueprint $table) {
            $table->increments('id');

            $table->json('chunk')->nullable();

            $table->uuid('case_context_id');
            $table->foreign('case_context_id')->references('id')->on('case_contexts')->onDelete('cascade');

            $table->integer('feature_set_id')->unsigned();
            $table->foreign('feature_set_id')->references('id')->on('feature_sets')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case_feature_chunks');
    }
}
