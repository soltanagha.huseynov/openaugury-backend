<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCaseContextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_contexts', function (Blueprint $table) {
            $table->uuid('id');

            $table->integer('district_id')->unsigned();
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade');
            $table->string('name');
            $table->point('center')->nullable();
            $table->polygon('extent')->nullable();
            $table->dateTime('begins');
            $table->dateTime('ends');

            $table->uuid('owner_id')->nullable();
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('set null');

            $table->timestamps();
            $table->primary('id');

	    //Necessary columns for Twill
	    $table->dateTime('deleted_at')->nullable();
	    $table->integer('position')->default(0);
	    $table->integer('published')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case_contexts');
    }
}
