<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureArcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_arcs', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('feature_chunk_id')->unsigned();
            $table->foreign('feature_chunk_id')->references('id')->on('case_feature_chunks')->onDelete('cascade');

            $table->uuid('simulation_id');
            $table->foreign('simulation_id')->references('id')->on('simulations')->onDelete('cascade');

            $table->json('arc');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_arcs');
    }
}
