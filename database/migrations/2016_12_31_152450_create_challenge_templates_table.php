<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengeTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenge_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();

            $table->string('text');
            $table->text('subtext')->default('');
            $table->string('challenge_type');
            $table->json('options')->nullable();
            $table->json('correct')->nullable();
            $table->decimal('mark')->default(0);
            $table->boolean('global')->default(true);

            $table->integer('analytic_id')->unsigned()->nullable();
            $table->foreign('analytic_id')->references('id')->on('analytics')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenge_templates');
    }
}
