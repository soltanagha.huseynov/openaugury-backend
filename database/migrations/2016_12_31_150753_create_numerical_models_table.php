<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNumericalModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numerical_models', function (Blueprint $table) {
            $table->uuid('id');

            $table->string('name');
            $table->text('definition');

            $table->integer('phenomenon_id')->unsigned();
            $table->foreign('phenomenon_id')->references('id')->on('phenomenons')->onDelete('cascade');

            $table->uuid('owner_id')->nullable();
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('set null');

            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numerical_models');
    }
}
