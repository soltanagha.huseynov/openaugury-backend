<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informationals', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('text');
            $table->text('subtext');
            $table->bigInteger('time_offset')->nullable();
            $table->string('author')->nullable();
            $table->json('metadata')->nullable();

            $table->integer('informational_type_id')->unsigned();
            $table->foreign('informational_type_id')->references('id')->on('informational_types')->onDelete('cascade');

            $table->uuid('simulation_id');
            $table->foreign('simulation_id')->references('id')->on('simulations')->onDelete('cascade');

            $table->uuid('creator_id')->nullable();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('set null');

            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informationals');
    }
}
