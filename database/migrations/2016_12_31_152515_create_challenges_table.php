<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenges', function (Blueprint $table) {
            $table->uuid('id');

            $table->string('text');
            $table->text('subtext')->default('');
            $table->string('challenge_type');
            $table->bigInteger('time_offset')->nullable();
            $table->json('options')->nullable();
            $table->json('correct')->nullable();
            $table->decimal('mark')->default(0);

            $table->integer('challenge_template_id')->unsigned();
            $table->foreign('challenge_template_id')->references('id')->on('challenge_templates')->onDelete('cascade');

            $table->uuid('simulation_id');
            $table->foreign('simulation_id')->references('id')->on('simulations')->onDelete('cascade');

            $table->integer('result_analytic_id')->unsigned()->nullable();
            $table->foreign('result_analytic_id')->references('id')->on('result_analytics')->onDelete('cascade');

            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenges');
    }
}
