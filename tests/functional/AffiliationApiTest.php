<?php

use App\Models\Users\Affiliation;
use Symfony\Component\HttpFoundation\Response;

class AffiliationApiTest extends FunctionalApiTest
{
    public static function getClass()
    {
        return Affiliation::class;
    }

    public function getTransformer()
    {
        return new AffiliationTransformer;
    }

    public function getJsonFields()
    {
        return [
            'name',
            'district_id',
        ];
    }

    /**
     * @test
     */
    public function testCreateAffiliation()
    {
        $affiliation = $this->fakeData();

        $affiliationJson = $this->reduceToJsonFields($affiliation);

        $this->json('POST', '/api/v1/affiliations', $affiliationJson);

        $this->assertApiResponse($affiliationJson);
    }

    /**
     * @test
     */
    public function testReadAffiliation()
    {
        $affiliation = $this->make();

        $affiliationJson = $this->reduceToJsonFields($affiliation->toArray());

        $this->json('GET', '/api/v1/affiliations/' . $affiliation->id);

        $this->assertApiResponse($affiliationJson);
    }

    /**
     * @test
     */
    public function testUpdateAffiliation()
    {
        $affiliation = $this->make();

        $editedAffiliation = $this->fakeData();

        $editedAffiliation = $this->reduceToJsonFields($editedAffiliation);

        $this->json('PUT', '/api/v1/affiliations/'.$affiliation->id, $editedAffiliation);

        $this->assertApiResponse($editedAffiliation);
    }

    /**
     * @test
     */
    public function testDeleteAffiliation()
    {
        $affiliation = $this->make();

        $this->json('DELETE', '/api/v1/affiliations/' . $affiliation->id);

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);

        $this->json('GET', '/api/v1/affiliations/' . $affiliation->id);

        $this->assertResponseStatus(Response::HTTP_NOT_FOUND);
    }
}
