<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use GeoJSON\GeoJSON;
use Webpatser\Uuid\Uuid;

use App\Transformers\FeatureArcTransformer;
use App\Models\SimulationTier\FeatureArc;
use App\Models\SimulationTier\Simulation;
use App\Models\CaseTier\CaseFeatureChunk;

class FeatureArcTransformerTest extends FunctionalApiTest
{
    public static function getClass()
    {
        return FeatureArc::class;
    }

    public function getTransformer()
    {
        return new FeatureArcTransformer;
    }

    /**
     * Ensure that a featureArc can provide standard information in JSON.
     *
     * @return void
     */
    public function testFeatureArcHasBasicInfo()
    {
        $featureArc = $this->fake();

        $transformer = new FeatureArcTransformer;
        $response = json_encode($transformer->transform($featureArc));

        $value = json_decode($response);

        $this->assertEquals($value->id, $featureArc->id);
        $this->assertEquals($value->created_at, json_decode(json_encode($featureArc->created_at)));
        $this->assertEquals($value->updated_at, json_decode(json_encode($featureArc->updated_at)));
    }

    /**
     * Check that the phenomenon ID is included with a featureArc
     *
     * @return void
     */
    public function testFeatureArcHasSimulationId()
    {
        $featureArc = $this->fake();

        $simulation = new Simulation;
        $simulation->id = (string)Uuid::generate();

        $featureArc->simulation = $simulation;
        $featureArc->simulation_id = $simulation->id;

        $transformer = new FeatureArcTransformer;
        $response = json_encode($transformer->transform($featureArc));

        $value = json_decode($response);

        $this->assertEquals($value->simulation_id, $simulation->id);
    }

    /**
     * Check that the case context ID is included with a featureArc
     *
     * @return void
     */
    public function testFeatureArcHasCaseContextId()
    {
        $featureArc = $this->fake();

        $featureChunk = new CaseFeatureChunk;
        $featureChunk->id = (string)Uuid::generate();

        $featureArc->featureChunk = $featureChunk;
        $featureArc->feature_chunk_id = $featureChunk->id;

        $transformer = new FeatureArcTransformer;
        $response = json_encode($transformer->transform($featureArc));

        $value = json_decode($response);

        $this->assertEquals($value->feature_chunk_id, $featureChunk->id);
    }
}
