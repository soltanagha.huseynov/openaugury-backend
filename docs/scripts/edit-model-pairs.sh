#!/bin/zsh

function namespaceFromFile {
    grep -h 'namespace' $1 | sed 's/^namespace \(.*\);/\1/g' | sed 's/\\/\\\\/g'
}

setopt extended_glob

filelist=$(for file in app/Models/*.php; do echo `expr length $file` $file; done | sort -rn)

for file in app/Models/*.php
do
    modelFile=$(basename $file)
    modelName=$(echo $modelFile | cut -d. -f1)
    otherModelFile=$(find app/Models -iname $modelFile ! -path $file)
    otherNamespace=$(namespaceFromFile $otherModelFile)
    originalNamespace=$(namespaceFromFile $file)
    otherFullName=$otherNamespace\\\\$modelName
    originalFullName=$originalNamespace\\\\$modelName
    mv $otherModelFile $otherModelFile.bak
    mv $file $otherModelFile
    sed -i "s#namespace ${originalNamespace}#namespace ${otherNamespace}#g" $otherModelFile
done
