<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('client');
//});


Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/help', function () {
    return Response::view('help.index');
});
Route::get('/help/{page}', function ($page) {
    return Response::view('help.' . $page);
});


Route::get('/register', 'UserController@createRegister');
Route::post('/register', ['uses' => 'UserController@storeRegister', 'as' => 'users.store-register']);

Route::post('/affiliations/{affiliationId}/add-simulations-30', 'AffiliationController@updateAddSimulations10');

//Route::get('/panel/CaseTier:CaseContext/downloadReport/{id}', 'CaseTier\CaseContextController@downloadReport');
//Route::get('/panel/SimulationTier:Simulation/downloadReport/{id}', 'SimulationTier\SimulationController@downloadReport');

Auth::routes();

Route::get('/', function () {
    return redirect('/home');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@get')->name('profile');

/*Route::group([
    'prefix' => 'oauth',
    'namespace' => '\App\Http\Controllers\Passport',
], function ($router) {
    (new \App\RouteRegistrar($router))->all();
});*/
