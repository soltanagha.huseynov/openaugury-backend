<?php

return [
 'auth_login_redirect_path' => '/admin',
 'admin_app_url' => 'localhost',
 'users_table' => 'users',
 'admin_app_path' => 'admin',
 'enabled' => [
     'users-management' => false
 ],
];
