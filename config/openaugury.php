<?php

return [
    'anonymous-access' => true,
    'brand' => [
        'slug' => 'openaugury',
        'name' => 'OpenAugury',
    ],
    'simulation' => [
        'server' => env('SIMULATION_SERVER', '172.17.0.1'),
        'port' => env('SIMULATION_SERVER_PORT', '8080'),
        'maximum_decode_memory' => env('SIMULATION_MAXIMUM_DECODE_MEMORY', '1000M')
    ],
    'legal' => [
        'terms-and-conditions-link' => 'https://s3.eu-west-2.amazonaws.com/ourragingplanet-media/subscription-terms-conditions.pdf',
        'privacy-policy-link' => 'https://s3.eu-west-2.amazonaws.com/ourragingplanet-media/privacy-and-cookies-policy.pdf'
    ],
    'frontend' => [
        'proxy' => env('FRONTEND_PROXY', 'http://172.24.0.1:8080/'),
        'prefix' => 'static/'
    ]
];
