<?php

return [

        'simulations' => [
                'title' => 'Simulations',
                'can' => \App\Models\SimulationTier\Simulation::class,
                'module' => true
        ],

        'caseContexts' => [
                'title' => 'Target Zones',
                'module' => true
            ],

        'districts' => [
                'title' => 'Districts',
                'can' => '',
                'module' => true
            ],


];
