<?php

namespace App\Listeners;

use App\Events\SimulationCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateShareableTokenForSimulation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SimulationCreated  $event
     * @return void
     */
    public function handle(SimulationCreated $event)
    {
        $simulation = $event->getSimulation();

        $user = $simulation->owner;

        if ($user) {
            $user->createToken(
                'simulation.' . $simulation->id,
                ['simulation.' . $simulation->id . '.view']
            );
        }
    }
}
