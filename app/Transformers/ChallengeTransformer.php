<?php

namespace App\Transformers;

use App\Models\Interaction\Challenge;
use League\Fractal;

/**
 * @SWG\Definition(
 *      definition="Challenge",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="string",
 *          format="uuid"
 *      ),
 *      @SWG\Property(
 *          property="text",
 *          description="text",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtext",
 *          description="subtext",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="time",
 *          description="time",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="time_offset",
 *          description="time_offset",
 *          type="integer",
 *          format="int64"
 *      ),
 *      @SWG\Property(
 *          property="options",
 *          description="multi-choice options",
 *          type="string",
 *          format="json"
 *      ),
 *      @SWG\Property(
 *          property="correct",
 *          description="the correct option",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtext",
 *          description="subtext",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ChallengeTransformer extends Fractal\TransformerAbstract
{
    public function transform(Challenge $challenge)
    {
        $challengeArray = $challenge->toArray();

        return [
            'id' => $challengeArray['id'],
            'text' => $challengeArray['text'],
            'challenge_type' => $challengeArray['challenge_type'],
            'subtext' => $challengeArray['subtext'],
            'time' => $challengeArray['time'],
            'time_offset' => $challengeArray['time_offset'],
            'options' => json_decode($challengeArray['options']),
            'correct' => json_decode($challengeArray['correct']),
            'mark' => $challengeArray['mark'],
            'created_at' => $challengeArray['created_at'],
            'updated_at' => $challengeArray['updated_at']
        ];
    }
}
