<?php

namespace App\Transformers;

use App\Models\AbstractTier\Phenomenon;
use League\Fractal;

class PhenomenonTransformer extends Fractal\TransformerAbstract
{
    public function transform(Phenomenon $phenomenon)
    {
        $phenomenonArray = $phenomenon->toArray();

        return [
            'id' => $phenomenonArray['slug'],
            'name' => $phenomenonArray['name'],
            'symbol' => $phenomenonArray['symbol'],
            'color' => $phenomenonArray['color'],
            'updated_at' => $phenomenonArray['updated_at'],
            'created_at' => $phenomenonArray['created_at'],
        ];
    }
}
