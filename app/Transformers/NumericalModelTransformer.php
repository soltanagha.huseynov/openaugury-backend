<?php

namespace App\Transformers;

use App\Models\AbstractTier\NumericalModel;
use League\Fractal;

class NumericalModelTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
            'phenomenon'
    ];

    public function transform(NumericalModel $numericalModel)
    {
        $numericalModelArray = $numericalModel->toArray();

        return [
            'id' => $numericalModelArray['id'],
            'name' => $numericalModelArray['name'],
            'definition' => $numericalModelArray['definition']
        ];
    }

    public function includePhenomenon(NumericalModel $numericalModel)
    {
        return $this->item($numericalModel->phenomenon, new PhenomenonTransformer, 'phenomenons');
    }
}
