<?php

namespace App\Transformers;

use App\Models\Features\District;
use League\Fractal;

/**
 * @SWG\Definition(
 *      definition="District",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class DistrictTransformer extends Fractal\TransformerAbstract
{
    public function transform(District $district)
    {
        $districtArray = $district->toArray();

        return [
            'id' => $districtArray['slug'],
            'name' => $districtArray['name'],
            'created_at' => $districtArray['created_at'],
            'updated_at' => $districtArray['updated_at']
        ];
    }
}
