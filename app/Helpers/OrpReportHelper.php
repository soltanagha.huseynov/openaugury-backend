<?php namespace App\Helpers;

use Illuminate\Support\Collection;
use App\Models\CachedDataServerFeatureSet;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\PhpWord\Style\Table;
use PhpOffice\PhpWord\IOFactory;
use Carbon\Carbon;
use SplTempFileObject;

class OrpReportHelper {
    /**
     * PHPWord document generator
     *
     * @var Phpword $_phpWord
     */
    protected $phpWord;

    public function __construct() {
        $this->phpWord = new PhpWord;
		$section = $this->phpWord->addSection();
		$section->addImage('https://gitlab.com/openaugury/openaugury-frontend/raw/master/images/logo-text.png');
        static::addStyles($this->phpWord);
	}

    protected static function addStyles(PhpWord $phpWord) {
        $phpWord->addTitleStyle(1, ['size' => 10, 'color' => '006080', 'bold' => false]);
	}

    protected static function writeToFileObject(PhpWord $phpWord)
    {
        $tmpFile = tempnam(sys_get_temp_dir(), 'orp-report');
        $writer = IOFactory::createWriter($phpWord, 'HTML');

        $writer->save($tmpFile);
        return $tmpFile;
	}

	public function createSimulationFeaturesTable($simulationModel){
			$this::createFeaturesTable($simulationModel->caseContext);
	}

	public function createFeaturesTable($caseContext){
		$sectionFeatures = $this->phpWord->addSection();

		$simulationFeaturesTable = $sectionFeatures->addTable();
		// for each loop to add each feature set to the table, as well as the count of objects within the simulation area of effect
	    $features = $caseContext->caseFeatureChunks->each(function ($featureArc) use (&$sectionFeatures, &$simulationFeaturesTable) {
			$path = base_path('resources/assets/images/markers/');
			$featureSetIcon = $path . $featureArc->featureSet->slug . '.png';
			$featureCount = (int) filter_var($featureArc->stats, FILTER_SANITIZE_NUMBER_INT);
			if (file_exists($featureSetIcon) && $featureCount != 0) {
				if ($featureCount != 0) {
						$simulationFeaturesTable->addRow();
						$simulationFeaturesTable->addCell(array('borderSize' => 10, 'borderLeftColor' => 'FF0000'))->addImage($featureSetIcon, array('height' => 40));
						$simulationFeaturesTable->addCell()->addText($featureArc->featureSet->name, array('color' => '000000', 'size' => 10));
						$simulationFeaturesTable->addCell()->addText($featureCount, array('color' => '000000', 'size' => 10));
				}
			}
			if(!file_exists($featureSetIcon)){
					$simulationFeaturesTable->addCell()->addText($featureArc->featureSet->name, array('color' => '000000', 'size' => 10));
			}
		});
	}

	public function createSimulationCaseContextTable($simulationModel){
		$this::createCaseContextTable($simulationModel->caseContext);
	}

	public function createCaseContextTable($caseContext){
		//create the section for the general case information
		$sectionCaseInformation = $this->phpWord->addSection();
		$caseInfoTable = $sectionCaseInformation->addTable(array('borderSize' => 10, 'borderColor' => '999999'));
		$caseInfoTable->addRow();
		$caseNameTitleCell = $caseInfoTable->addCell(300)->addTitle('Case');
		$caseNameDetailsCell = $caseInfoTable->addCell(300)->addText($caseContext->name);
		$caseInfoTable->addRow();
		$reportDateTitleCell = $caseInfoTable->addCell(300)->addTitle('Date of Report');
		$reportDateInfoCell = $caseInfoTable->addCell(300)->addText(Carbon::now()->format('d-m-Y'));
		$caseInfoTable->addRow();
		$ownerNameTitleCell = $caseInfoTable->addCell(300)->addTitle("Owner");
		$ownerNameInfoCell = $caseInfoTable->addCell(300)->addText(($caseContext->owner ? $caseContext->owner->name : "(none)"));
		$caseInfoTable->addRow();
		$dataDistrictTitleCell = $caseInfoTable->addCell(300)->addTitle("Data District");
		$dataDistrictInfoCell = $caseInfoTable->addCell(300)->addText($caseContext->district->name ? $caseContext->district->slug : "(none)");
		//co-ordinates for the center of the disaster
		$caseInfoTable->addRow();
        $latLngOutput = $caseContext->centerLatLng;
        $latLongTitleCell = $caseInfoTable->addCell(300)->addTitle("Phenomenon Centre");
		$coordsInfoCell = $caseInfoTable->addCell(300)->addText("(" . $latLngOutput[0] . ", " . $latLngOutput[1] . ")");
		//the duration (dates and times)
		$caseInfoTable->addRow();
        if ($caseContext->begins) {
            $simulationDurationFormatting = $caseContext->begins->format('d-m-Y H:i:s') . " - " . $caseContext->ends->format('d-m-Y H:i:s');
            $simulationDurationTitleCell = $caseInfoTable->addCell(300)->addTitle("Duration of phenomenon"); 
			$simDurationInfoCell = $caseInfoTable->addCell(300)->addText($simulationDurationFormatting, array('borderTopColor' => 'FF0000'));
		}
		$sectionCaseInformation->addTextBreak(1);
		//refactoring code
	}

    public function exportToTemporaryFile($caseContext) {
				$caseContextTable = $this::createCaseContextTable($caseContext);
				//add the information for the features in the simulation
				$featuresTable = $this::createFeaturesTable($caseContext);
				return static::writeToFileObject($this->phpWord);
		}

    public function exportSimulationReportToTemporaryFile($simulationModel) {
				$caseSimContextTable = $this::createSimulationCaseContextTable($simulationModel);
				//add the information for the features in the simulation
				$featuresSimTable = $this::createSimulationFeaturesTable($simulationModel);
				return static::writeToFileObject($this->phpWord);
    }
}
