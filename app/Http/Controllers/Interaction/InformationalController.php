<?php

namespace App\Http\Controllers\Interaction;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

use Auth;
use App\Models\Interaction\Informational;
use App\Models\Interaction\InformationalType;
use App\Models\SimulationTier\Simulation;
use App\Jobs\InformationalRun;

class InformationalController extends CrudController
{

    public function __construct(\Lang $lang, Request $request)
    {
        parent::__construct($lang);

        $this->request = $request;
        $this->features['all']['import-button'] = false;
        $this->features['all']['export-button'] = false;
    }

    public function all($entity)
    {
        parent::all($entity);

        $informationalModel = new Informational;
        $informationalModel->with('simulation');

        if (!Auth::user()->hasRole('admin')) {
            $informationalModel = $informationalModel->whereHas('simulation', function ($q) {
                $q->whereOwnerId(Auth::user()->id);
            });
        }

        $addUrl = null;
        if ($this->request->has('simulation_id')) {
            $simulationId = $this->request->get('simulation_id');
            $addUrl = url('panel', $entity . '/edit') . '?simulation_id=' . $this->request->get('simulation_id');
            $informationalModel = $informationalModel->whereSimulationId($simulationId);

            $this->features['all']['return-button'] = [
                'target' => url('panel', 'SimulationTier:Simulation/edit') . '?modify=' . $simulationId,
                'label' => 'Return to Simulation'
            ];
        }

        $this->filter = \DataFilter::source($informationalModel);

        $this->grid = \DataGrid::source($this->filter);
        
        $this->grid->add_url = $addUrl;
        $this->grid->add('{{ $time_offset > 0 ? "+" : "" }}{{ round($time_offset / 3600) }}h {{ abs(round($time_offset / 60) % 60) }}m', 'Time');
        $this->grid->add('text', 'Title');
        $this->grid->add('subtext', 'Content');
        $this->grid->add('time', 'Event Time');

        $this->addStylesToGrid();

        $this->grid->orderBy('time_offset');

        return $this->returnView('panelViews::all', ['title' => 'Informationals (Info, News, Social Media)']);
    }

    public function edit($entity) {

        parent::edit($entity);

        $this->edit = \DataEdit::source(new Informational);

        if ($this->request->has('simulation_id')) {
            $simulationId = $this->request->get('simulation_id');
        } else {
            if (!$this->edit->model || !$this->edit->model->simulation_id) {
                // TODO: eliminate or implement this route
                abort(404);
            } else {
                $simulationId = $this->edit->model->simulation_id;
            }
        }

        $simulation = Simulation::findOrFail($simulationId);

        $this->edit->label('Edit Informational');

        $this->edit->add('informational_type_id', 'Type', 'select')
            ->options(InformationalType::pluck('name', 'id'));

        $this->edit->add('simulation_id', 'Simulation', 'hidden')->insertValue($simulation->id);
        $this->edit->add('text', 'Title', 'text');
        $this->edit->add('author', 'Author to Display', 'text');
        $this->edit->add('time_offset', 'Seconds after simulation start<br/>(Start is ' . $simulation->begins . ')', 'text')->rule('integer');

        if ($this->edit->model) {
        }

        $this->edit->add('subtext', 'Content', 'textarea');

        $this->edit->saved(function ($edit) use ($entity, $simulation) {
            return redirect(url('panel', $entity . '/all') . '?simulation_id=' . $simulation->id);
        });

        return $this->returnEditView('panelViews::edit', ['title' => 'Edit Informational (Info, News, Social Media)']);
    }
}
