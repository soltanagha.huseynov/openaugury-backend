<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class SimulationController extends ModuleController
{
    protected $moduleName = 'simulations';

    protected $titleColumnKey = 'name';

    protected $indexColumns = [
        'name' => [ // field column
            'title' => 'Title',
            'field' => 'name',
        ],
        'target_zone' => [ // field column
            'title' => 'Target Zone',
            'field' => 'case_context_id',
        ],
	'from' => [ // field column
            'title' => 'From',
            'field' => 'begins',
        ],
	'to' => [ // field column
            'title' => 'To',
            'field' => 'ends',
        ],
        'created' => [ // field column
            'title' => 'Created',
            'field' => 'created_at',
        ],
        'updated' => [ // field column
            'title' => 'Updated',
            'field' => 'updated_at',
        ],
        'status' => [ // field column
            'title' => 'Status',
            'field' => 'status',
        ],
    ];

}
