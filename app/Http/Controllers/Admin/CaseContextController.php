<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class CaseContextController extends ModuleController
{
    protected $moduleName = 'caseContexts';

    protected $titleColumnKey = 'name';

    protected $indexColumns = [
        'name' => [ // field column
            'title' => 'Name',
            'field' => 'name',
        ],
        'email' => [ // field column
            'title' => 'Created',
            'field' => 'created_at',
        ],
	'register' => [ // field column
            'title' => 'Updated',
            'field' => 'updated_at',
        ],
        'switch' => [ // field column
            'title' => 'Edit',
            'field' => 'edit',
        ],
    ];

}
