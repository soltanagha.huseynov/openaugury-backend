<?php

namespace App\Repositories;

use App\Models\Users\Affiliation;
use InfyOm\Generator\Common\BaseRepository;

class AffiliationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'name',
        'district_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Affiliation::class;
    }

    /**
     * Create a metered version of the model
     */
    public function createMetered($attributes)
    {
        $model = self::create($attributes);
        $model->setMetered(0);
        $model->save();
        return $model;
    }
}
