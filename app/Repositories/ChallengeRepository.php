<?php

namespace App\Repositories;

use App\Models\Interaction\Challenge;
use InfyOm\Generator\Common\BaseRepository;

class ChallengeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'text',
        'subtext',
        'options',
        'correct',
        'mark',
        'challenge_template_id',
        'simulation_id',
        'result_analytic_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Challenge::class;
    }
}
