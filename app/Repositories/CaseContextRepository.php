<?php

namespace App\Repositories;

use App\Models\CaseTier\CaseContext;
use InfyOm\Generator\Common\BaseRepository;
use A17\Twill\Repositories\ModuleRepository;

class CaseContextRepository extends ModuleRepository
{

    public function __construct(CaseContext $model)
    {
        $this->model = $model;
    }


    /**
     * @var array
     */
    protected $fieldSearchable = [
        'district_id',
        'name',
        'center',
        'extent',
        'begins',
        'ends',
        'owner_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CaseContext::class;
    }
}
