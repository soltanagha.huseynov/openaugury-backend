<?php

namespace App\Models;

use Eloquent as Model;


/**
 * @SWG\Definition(
 *      definition="CachedDataServerFeatureSet",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="owner",
 *          description="owner",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="license_title",
 *          description="license_title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="license_url",
 *          description="license_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="uri",
 *          description="uri",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="data_server",
 *          description="data_server",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="data_server_set_id",
 *          description="data_server_set_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CachedDataServerFeatureSet extends Model
{


    public $table = 'cached_data_server_feature_sets';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'name',
        'owner',
        'license_title',
        'license_url',
        'uri',
        'data_server',
        'data_server_set_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'owner' => 'string',
        'license_title' => 'string',
        'license_url' => 'string',
        'uri' => 'string',
        'data_server' => 'string',
        'data_server_set_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cachedDataServerFeatures()
    {
        return $this->hasMany(\App\Models\CachedDataServerFeature::class);
    }
}
