<?php

namespace App\Models;

use Eloquent as Model;

use Phaza\LaravelPostgis\Eloquent\PostgisTrait;

/**
 * @SWG\Definition(
 *      definition="CachedDataServerFeature",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cached_data_server_feature_set_id",
 *          description="cached_data_server_feature_set_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="feature_id",
 *          description="feature_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location",
 *          description="location",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="extent",
 *          description="extent",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CachedDataServerFeature extends Model
{

    use PostgisTrait;

    public $table = 'cached_data_server_features';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'cached_data_server_feature_set_id',
        'feature_id',
        'location',
        'extent',
        'json'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cached_data_server_feature_set_id' => 'integer',
        'feature_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Fields that contain GIS data
     *
     * @var array
     */
    public $postgisFields = [
        'location',
        'extent'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cachedDataServerFeatureSet()
    {
        return $this->belongsTo(\App\Models\CachedDataServerFeatureSet::class, 'cached_data_server_feature_set_id', 'id');
    }
}
