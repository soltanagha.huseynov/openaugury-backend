<?php return array (
  'area17/twill' => 
  array (
    'providers' => 
    array (
      0 => 'A17\\Twill\\TwillServiceProvider',
    ),
  ),
  'astrotomic/laravel-translatable' => 
  array (
    'providers' => 
    array (
      0 => 'Astrotomic\\Translatable\\TranslatableServiceProvider',
    ),
  ),
  'cartalyst/tags' => 
  array (
    'providers' => 
    array (
      0 => 'Cartalyst\\Tags\\TagsServiceProvider',
    ),
  ),
  'cviebrock/eloquent-sluggable' => 
  array (
    'providers' => 
    array (
      0 => 'Cviebrock\\EloquentSluggable\\ServiceProvider',
    ),
  ),
  'graham-campbell/flysystem' => 
  array (
    'providers' => 
    array (
      0 => 'GrahamCampbell\\Flysystem\\FlysystemServiceProvider',
    ),
  ),
  'infyomlabs/adminlte-templates' => 
  array (
    'providers' => 
    array (
      0 => '\\InfyOm\\AdminLTETemplates\\AdminLTETemplatesServiceProvider',
    ),
  ),
  'infyomlabs/laravel-generator' => 
  array (
    'providers' => 
    array (
      0 => '\\InfyOm\\Generator\\InfyOmGeneratorServiceProvider',
    ),
  ),
  'intervention/image' => 
  array (
    'providers' => 
    array (
      0 => 'Intervention\\Image\\ImageServiceProvider',
    ),
    'aliases' => 
    array (
      'Image' => 'Intervention\\Image\\Facades\\Image',
    ),
  ),
  'laracasts/flash' => 
  array (
    'providers' => 
    array (
      0 => 'Laracasts\\Flash\\FlashServiceProvider',
    ),
    'aliases' => 
    array (
      'Flash' => 'Laracasts\\Flash\\Flash',
    ),
  ),
  'laravel-doctrine/orm' => 
  array (
    'providers' => 
    array (
      0 => 'LaravelDoctrine\\ORM\\DoctrineServiceProvider',
    ),
    'aliases' => 
    array (
      'Registry' => 'LaravelDoctrine\\ORM\\Facades\\Registry',
      'Doctrine' => 'LaravelDoctrine\\ORM\\Facades\\Doctrine',
      'EntityManager' => 'LaravelDoctrine\\ORM\\Facades\\EntityManager',
    ),
  ),
  'laravel/cashier' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Cashier\\CashierServiceProvider',
    ),
  ),
  'laravel/passport' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Passport\\PassportServiceProvider',
    ),
  ),
  'laravel/sanctum' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Sanctum\\SanctumServiceProvider',
    ),
  ),
  'laravel/socialite' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Socialite\\SocialiteServiceProvider',
    ),
    'aliases' => 
    array (
      'Socialite' => 'Laravel\\Socialite\\Facades\\Socialite',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'laravel/ui' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Ui\\UiServiceProvider',
    ),
  ),
  'laravelcollective/html' => 
  array (
    'providers' => 
    array (
      0 => 'Collective\\Html\\HtmlServiceProvider',
    ),
    'aliases' => 
    array (
      'Form' => 'Collective\\Html\\FormFacade',
      'Html' => 'Collective\\Html\\HtmlFacade',
    ),
  ),
  'maatwebsite/excel' => 
  array (
    'providers' => 
    array (
      0 => 'Maatwebsite\\Excel\\ExcelServiceProvider',
    ),
    'aliases' => 
    array (
      'Excel' => 'Maatwebsite\\Excel\\Facades\\Excel',
    ),
  ),
  'matthewbdaly/laravel-azure-storage' => 
  array (
    'providers' => 
    array (
      0 => 'Matthewbdaly\\LaravelAzureStorage\\AzureStorageServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'phaza/laravel-postgis' => 
  array (
    'providers' => 
    array (
      0 => 'Phaza\\LaravelPostgis\\DatabaseServiceProvider',
    ),
  ),
  'prettus/l5-repository' => 
  array (
    'providers' => 
    array (
      0 => 'Prettus\\Repository\\Providers\\RepositoryServiceProvider',
    ),
  ),
  'spatie/laravel-activitylog' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\Activitylog\\ActivitylogServiceProvider',
    ),
  ),
  'spatie/laravel-analytics' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\Analytics\\AnalyticsServiceProvider',
    ),
    'aliases' => 
    array (
      'Analytics' => 'Spatie\\Analytics\\AnalyticsFacade',
    ),
  ),
  'spatie/laravel-fractal' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\Fractal\\FractalServiceProvider',
    ),
    'aliases' => 
    array (
      'Fractal' => 'Spatie\\Fractal\\FractalFacade',
    ),
  ),
  'spatie/laravel-permission' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\Permission\\PermissionServiceProvider',
    ),
  ),
  'swisnl/json-api-client' => 
  array (
    'providers' => 
    array (
      0 => 'Swis\\JsonApi\\Client\\Providers\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'DocumentFactory' => 'Swis\\JsonApi\\Client\\Facades\\DocumentFactoryFacade',
      'DocumentParser' => 'Swis\\JsonApi\\Client\\Facades\\DocumentParserFacade',
      'ItemHydrator' => 'Swis\\JsonApi\\Client\\Facades\\ItemHydratorFacade',
      'ResponseParser' => 'Swis\\JsonApi\\Client\\Facades\\ResponseParserFacade',
      'TypeMapper' => 'Swis\\JsonApi\\Client\\Facades\\TypeMapperFacade',
    ),
  ),
  'webpatser/laravel-uuid' => 
  array (
    'aliases' => 
    array (
      'Uuid' => 'Webpatser\\Uuid\\Uuid',
    ),
  ),
);