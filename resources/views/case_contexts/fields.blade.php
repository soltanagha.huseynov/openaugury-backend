<!-- District Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('district_id', 'District Id:') !!}
    {!! Form::number('district_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Center Field -->
<div class="form-group col-sm-6">
    {!! Form::label('center', 'Center:') !!}
    {!! Form::text('center', null, ['class' => 'form-control']) !!}
</div>

<!-- Extent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('extent', 'Extent:') !!}
    {!! Form::text('extent', null, ['class' => 'form-control']) !!}
</div>

<!-- Begins Field -->
<div class="form-group col-sm-6">
    {!! Form::label('begins', 'Begins:') !!}
    {!! Form::date('begins', null, ['class' => 'form-control']) !!}
</div>

<!-- Ends Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ends', 'Ends:') !!}
    {!! Form::date('ends', null, ['class' => 'form-control']) !!}
</div>

<!-- Owner Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    {!! Form::text('owner_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('caseContexts.index') !!}" class="btn btn-default">Cancel</a>
</div>
