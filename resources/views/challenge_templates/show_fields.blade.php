<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $challengeTemplate->id !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $challengeTemplate->slug !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $challengeTemplate->text !!}</p>
</div>

<!-- Subtext Field -->
<div class="form-group">
    {!! Form::label('subtext', 'Subtext:') !!}
    <p>{!! $challengeTemplate->subtext !!}</p>
</div>

<!-- Options Field -->
<div class="form-group">
    {!! Form::label('options', 'Options:') !!}
    <p>{!! $challengeTemplate->options !!}</p>
</div>

<!-- Correct Field -->
<div class="form-group">
    {!! Form::label('correct', 'Correct:') !!}
    <p>{!! $challengeTemplate->correct !!}</p>
</div>

<!-- Mark Field -->
<div class="form-group">
    {!! Form::label('mark', 'Mark:') !!}
    <p>{!! $challengeTemplate->mark !!}</p>
</div>

<!-- Global Field -->
<div class="form-group">
    {!! Form::label('global', 'Global:') !!}
    <p>{!! $challengeTemplate->global !!}</p>
</div>

<!-- Analytic Id Field -->
<div class="form-group">
    {!! Form::label('analytic_id', 'Analytic Id:') !!}
    <p>{!! $challengeTemplate->analytic_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $challengeTemplate->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $challengeTemplate->updated_at !!}</p>
</div>

