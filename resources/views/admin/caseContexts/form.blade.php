@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'name',
        'label' => 'Name',
        'maxlength' => 100
    ])


    @formField('checkboxes', [
    'name' => 'feature_set_id',
    'label' => 'Feature Sets',
    'inline' => true,
    'options' => [
        [
            'value' => 'listed-buildings',
            'label' => 'Listed Buildings'
        ],
        [
            'value' => 'regional-waters',
            'label' => 'Regional Waters'
        ],
        [
            'value' => 'street-light',
            'label' => 'Street Light'
        ],
        [
            'value' => 'unexplored-cultural-heritage',
            'label' => 'Unexplored Cultural Heritage'
        ],
        [
            'value' => 'industrial-heritage',
            'label' => 'Industrial Heritage'
        ],
        [
            'value' => 'sports-facility',
            'label' => 'Sports Facility'
        ],
        [
            'value' => 'bus-stop',
            'label' => 'Bus Stop'
        ],
        [
            'value' => 'school-locations',
            'label' => 'School Locations'
        ],
        [
            'value' => 'focal-point',
            'label' => 'Focal Point'
        ],
    ]
    ])

    @formField('select', [
        'name' => 'district',
        'label' => 'District',
	'placeholder' => 'Select a district',
	'options' => $district,
        'maxlength' => 100
    ])


    @formField('map', [
    'name' => 'center',
    'label' => 'Centre',
    'showMap' => true,
    ])

   @formField('date_picker', [
    'name' => 'begins',
    'label' => 'Beginning Time',
    //'minDate' => '2017-09-10 12:00',
    //'maxDate' => '2017-12-10 12:00'
    ])

    @formField('date_picker', [
    'name' => 'ends',
    'label' => 'Ending Time',
    //'minDate' => '2017-09-10 12:00',
    //'maxDate' => '2017-12-10 12:00'
    ])

@stop
