<table class="table table-responsive" id="affiliations-table">
    <thead>
        <th>Slug</th>
        <th>Name</th>
        <th>District Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($affiliations as $affiliation)
        <tr>
            <td>{!! $affiliation->slug !!}</td>
            <td>{!! $affiliation->name !!}</td>
            <td>{!! $affiliation->district_id !!}</td>
            <td>
                {!! Form::open(['route' => ['affiliations.destroy', $affiliation->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('affiliations.show', [$affiliation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('affiliations.edit', [$affiliation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>