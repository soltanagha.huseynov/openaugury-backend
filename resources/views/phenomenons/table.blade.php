<table class="table table-responsive" id="phenomenons-table">
    <thead>
        <th>Slug</th>
        <th>Name</th>
        <th>Symbol</th>
        <th>Color</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($phenomenons as $phenomenon)
        <tr>
            <td>{!! $phenomenon->slug !!}</td>
            <td>{!! $phenomenon->name !!}</td>
            <td>{!! $phenomenon->symbol !!}</td>
            <td>{!! $phenomenon->color !!}</td>
            <td>
                {!! Form::open(['route' => ['phenomenons.destroy', $phenomenon->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('phenomenons.show', [$phenomenon->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('phenomenons.edit', [$phenomenon->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>