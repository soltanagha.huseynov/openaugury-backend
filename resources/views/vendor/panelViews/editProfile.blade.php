
@extends('panelViews::mainTemplate')
@section('page-wrapper')

@if (!empty($message))
    <div class="alert-box success">
        <h2>{{ $message }}</h2>
    </div>
@endif

<div class="row">
    <div class="col-xs-4" >
      <div class="well well-lg">
        {!!
         Form::model($admin, array( $admin->id))
        !!}

        {!! Form::label('first_name', \Lang::get('panel::fields.FirstName')) !!}
        {!! Form::text('first_name', $admin->first_name, array('class' => 'form-control')) !!}
        <br />
        {!! Form::label('last_name', \Lang::get('panel::fields.LastName')) !!}
        {!! Form::text('last_name', $admin->last_name, array('class' => 'form-control')) !!}
        <br />
        <!-- email -->
        {!! Form::label('email', \Lang::get('panel::fields.email')) !!}
        {!! Form::email('email', $admin->email, array('class' => 'form-control')) !!}
        <br />
        {!! Form::submit(\Lang::get('panel::fields.updateProfile'), array('class' => 'btn btn-primary')) !!}

        {!! Form::close() !!}
      </div>

  </div>
</div>

@if (is_numeric($admin->affiliation->canSimulate()))
<div class="row">
    <div class="col-xs-4" >
      <div class="well well-lg">
        <!-- Remaining credits -->
          {{ Form::label('affiliation_simulations_remaining', 'Simulations remaining for ' . $admin->affiliation->name) }}:

          {{ $admin->affiliation->simulations_remaining }}
        <br />
        <p>Simulations are metered. Certain services, which are being expanded, may be temporarily exempt from paid metering; when stabilised, they will be metered in line with existing Pricing documentation.</p>
        {{ Form::open(['url' => '/affiliations/' . $admin->affiliation->id . '/add-simulations-30', 'method' => 'POST']) }}
          <script
            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="pk_test_JGFfP8reY3y9GDbZ2CAqay4X"
            data-amount="9000"
            data-name="Flax &amp; Teal Limited"
            data-label="Add 30 simulations"
            data-description="30 simulations"
            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
            data-locale="auto"
            data-zip-code="true"
            data-currency="gbp">
          </script>
        {{ Form::close() }}
        </form>
      </div>
  </div>
</div>
@endif

@stop
