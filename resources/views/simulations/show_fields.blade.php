<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $simulation->id !!}</p>
</div>

<!-- Case Context Id Field -->
<div class="form-group">
    {!! Form::label('case_context_id', 'Case Context Id:') !!}
    <p>{!! $simulation->case_context_id !!}</p>
</div>

<!-- Combination Id Field -->
<div class="form-group">
    {!! Form::label('combination_id', 'Combination Id:') !!}
    <p>{!! $simulation->combination_id !!}</p>
</div>

<!-- Settings Field -->
<div class="form-group">
    {!! Form::label('settings', 'Settings:') !!}
    <p>{!! $simulation->settings !!}</p>
</div>

<!-- Definition Field -->
<div class="form-group">
    {!! Form::label('definition', 'Definition:') !!}
    <p>{!! $simulation->definition !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $simulation->status !!}</p>
</div>

<!-- Center Field -->
<div class="form-group">
    {!! Form::label('center', 'Center:') !!}
    <p>{!! $simulation->center !!}</p>
</div>

<!-- Extent Field -->
<div class="form-group">
    {!! Form::label('extent', 'Extent:') !!}
    <p>{!! $simulation->extent !!}</p>
</div>

<!-- Begins Field -->
<div class="form-group">
    {!! Form::label('begins', 'Begins:') !!}
    <p>{!! $simulation->begins !!}</p>
</div>

<!-- Ends Field -->
<div class="form-group">
    {!! Form::label('ends', 'Ends:') !!}
    <p>{!! $simulation->ends !!}</p>
</div>

<!-- Result Field -->
<div class="form-group">
    {!! Form::label('result', 'Result:') !!}
    <p>{!! $simulation->result !!}</p>
</div>

<!-- Owner Id Field -->
<div class="form-group">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    <p>{!! $simulation->owner_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $simulation->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $simulation->updated_at !!}</p>
</div>

