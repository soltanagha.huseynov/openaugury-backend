@extends('help.template')
@section('content')

<h2>For students</h2>

<p>To get started, click the link given to you by your
teacher. When a box appears saying <em>Token</em> with letters and numbers in
it, click <em>Submit</em>. You should now see a map. If you do not, contact
your teacher.</p> <p>Your teacher will have chosen a place in Northern Ireland
for you to explore - it may be your home town, or a nearby town or city. In the
map view, you can zoom in to see roads, lakes and some buildings up close, or
zoom out to see where in Northern Ireland you are (or the world, if you go
further!). You can drag the map left, right, up and down to see what other
towns and villages are nearby.</p>

<p>In the map you may see some markers,
pointed at the bottom and round at the top. These help you find different types
of local landmark - perhaps a sports field, or a community centre. You can
click them to find out more. If you do, a box with text will appear - you can
make this disappear again by clicking it.</p>

<div class='help-figure'>
    <img src='/images/screenshots/screenshot-description.png'/>
    <p>Figure: Description of a location shown in the <em>Map Viewer</em></p>
</div>

<p>As well as the map, you will
see a timeline along the bottom of the screen - this is a sliding ruler that
lets you pick a day and time during a disaster to see what has happened
(perhaps which buildings are destroyed, or which are at risk). If you are
looking at a natural disaster, like a storm surge or earthquake, then your
teacher will have a chosen a certain day and time for the disaster to start.
When you first see the map, it will be set to the disaster start time. You can
move forward and back in time by moving the big green bar - the coloured-in
area shows where there is risk of damage to buildings and people. As you move
it forward a few hours by dragging the green bar, the coloured-in area will
usually get bigger - this means more places are likely to be damaged as time
goes on. You may also see the markers in the coloured-in area start to go red,
meaning they are becoming damaged. You can drag the green bar back to go to the
start of the disaster, and the markers should go back to their normal
colour.</p>

<p>The Current Time is always shown in the timeline, and in the
white box at the top of the map.</p>

<p>You can find out more about how
disasters unfold by clicking the big red <em>News Feed</em> button. This will
show social media posts, newsflashes and general info about what is happening
at different times after the disaster starts - as you click the white
<em>Next</em> button, it will move you to the time of the next post in your
News Feed. As you go through the <em>News Feed</em> posts, you will see the
time in the white box at the top of the map change and the timeline move. Look
at the coloured-in area and the markers, they will change too as you go to the
next <em>News Feed</em> post.</p>

<p>Your teacher may have set you tasks. If
so, you can see them by clicking the big green <em>Tasks</em> button. If it is
a task to do on paper, or in the webpage, it will tell you what you should do.
If it is a multichoice question, pick the right answer from the choices
underneath. You can then click <em>Next Question</em> to get to the next
task.</p>

<p>If you need help, click the large Help button with the letter
<em>i</em> in it and this will explain how the website works.<!-- If you want help
from your teacher, click the large Feedback button with the <em>?</em> symbol -
this will help your teacher find you.--></p>

@endsection
