<!-- Resource Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('resource_id', 'Resource Id:') !!}
    {!! Form::number('resource_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Simulation Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('simulation_id', 'Simulation Id:') !!}
    {!! Form::text('simulation_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::number('quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', 'Duration:') !!}
    {!! Form::number('duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('resultResources.index') !!}" class="btn btn-default">Cancel</a>
</div>
