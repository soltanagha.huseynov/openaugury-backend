<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $resultResource->id !!}</p>
</div>

<!-- Resource Id Field -->
<div class="form-group">
    {!! Form::label('resource_id', 'Resource Id:') !!}
    <p>{!! $resultResource->resource_id !!}</p>
</div>

<!-- Simulation Id Field -->
<div class="form-group">
    {!! Form::label('simulation_id', 'Simulation Id:') !!}
    <p>{!! $resultResource->simulation_id !!}</p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('quantity', 'Quantity:') !!}
    <p>{!! $resultResource->quantity !!}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', 'Duration:') !!}
    <p>{!! $resultResource->duration !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $resultResource->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $resultResource->updated_at !!}</p>
</div>

