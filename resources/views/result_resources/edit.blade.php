@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Result Resource
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($resultResource, ['route' => ['resultResources.update', $resultResource->id], 'method' => 'patch']) !!}

                        @include('result_resources.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection